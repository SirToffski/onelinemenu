#!/bin/bash

pipedoptions=""
while read -r readoption
do
  pipedoptions="$pipedoptions
$readoption"
done

pipedoptions="$(echo "$pipedoptions" | tail -n +2)"

[ -z "$pipedoptions" ] && echo "No options piped, aborting..." && exit 1

# onelinemenu
# create an interactive one line selector menu that will disappear as soon as an option is
#  selected to be set in a variable named as specified  (press ? in menu for info on usage)
# parameters in order:
#  1*selopt - name of the variable to declare with the selected option
#  2*prompt - text to display prefixing the menu options
#  3*options - list of options (separated by line breaks) from which to select an option
#  4*escapable - (optional) flag that determines if the menu can be escaped by pressing
#    'Esc' (selopt variable will be declared as empty). True by default.
function onelinemenu {
 selopt=$1
 prompt="$2"
 options="$3"
 escapable=true
 [ -n "$4" ] && [ "$4" = "false" ] && escapable=false
 linelength=$(tput cols 2>/dev/null) #try using the whole available screen width
 [ -z "$linelength" ] && linelength=100
 inputs=""
 cursor=1
 helptxt=$(cat <<HELPTXT
------- One Line Menu Help: --------------------------------------------------------------------------------------------
           [ Enter] - selects currently shown option *(unavailable if no option is given)
	    [ Esc ] - exit without selecting an option *(can be deactivated)
 [ Tab ] / [ Down ] - next option
             [ Up ] - previous option
          [ Right ] - forward 5 options
           [ Left ] - back 5 options
              [ ? ] - display help and list of currently available options  (press [ Esc ]+[ q ] to exit this screen)
	Type - search for an option (matching text)
v------ Below this point are the currently available options (use [ / ] and type a name to search ) -------------------v
HELPTXT
 )
 while true
 do
  grepinputs="$inputs"
  grepinputs="${grepinputs//\\/\\\\}" #escape \
  grepinputs="${grepinputs//\[/\\\[}" #escape [
  avoptions="$(echo "$options" | grep -i -- "$grepinputs")"
  option="$(echo "$avoptions" | head -$cursor | tail -1)"
  avno=0
  [ -n "$avoptions" ] && avno="$(echo "$avoptions" | wc -l)"
  menuline='\033[0;35m'"$prompt ["'\033[0;32m'"${inputs//\\/\\\\}"'\033[0;35m'"] ($cursor/$avno) : "'\033[0;36m'"$option"'\033[0m'
  curlen=${#menuline}
  curlen=$((curlen -47)) #5 color tags (47 chars) should not count
  if [[ $curlen -gt $linelength ]]
  then
   menuline="${menuline:0:$((linelength +37))}..."'\033[0m' #first 4 color tags (40 chars) should be added to the count and 3 last chars substracted (+40-3=+37)
  else
   for sp in $(seq $((curlen +1)) $linelength )
   do
    menuline="$menuline "
   done
  fi
  echo -ne "\r$menuline"
  read -r -s -N 1 input </dev/tty
  case "$input" in
   $'\x7f') #backspace
    [ -n "$inputs" ] && inputs="${inputs::-1}"
    cursor=1
    ;;
   $'\x09') #tab
    cursor=$((cursor+1)) && [[ $cursor -gt $avno ]] && cursor=1
    ;;
   $'\x1b'|'['|'O') #escape sequence (or repeated escape sequence)
    read -r -s -N 1 -t .1 stub </dev/tty
    if [ "$stub" = "" ]
    then
     if [ "$input" = $'\x1b' ] #esc
     then
      $escapable && option="" && break
     else #another single printable character
      inputs="$inputs$input"
      cursor=1
     fi
    fi
    { [ "$stub" = "[" ] || [ "$stub" = "O" ] ; } && read -r -s -N 1 stub </dev/tty
    case "$stub" in
     A) # up
      cursor=$((cursor-1)) && [[ $cursor -lt 1 ]] && cursor=$avno
      ;;
     B) # down
      cursor=$((cursor+1)) && [[ $cursor -gt $avno ]] && cursor=1
      ;;
     C) # right
      cursor=$((cursor+5)) && [[ $cursor -gt $avno ]] && cursor=1
      ;;
     D) # left
      cursor=$((cursor-5)) && [[ $cursor -lt 1 ]] && cursor=$avno
      ;;
    esac
    read -r -s -N 4 -t .1 </dev/tty # flush out other sequences
    ;;
   $'\x0a') #enter
    [ -n "$option" ] && break
    ;;
   '?')
    echo -e "$helptxt\n$avoptions" | less
    ;;
   [[:graph:]]|' ')
    inputs="$inputs$input"
    cursor=1
    ;;
  esac
 done
 emptyline=""
 for sp in $(seq 1 $linelength)
 do
  emptyline="$emptyline "
 done
 echo -ne "\r$emptyline\r"
 declare -g "$selopt"="$option"
}

prompttext="Select an option"
[ -n "$1" ] && prompttext="$1"

onelinemenu "selectedvariable" "$prompttext" "$pipedoptions"
echo "$selectedvariable"
